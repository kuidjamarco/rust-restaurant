#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.height * self.width
    }
}

fn main() {
    let mut user1 = User {
        username: String::from("@kdmark"),
        email: String::from("kuidjamarco@gmail.com"),
        sign_in_count: 1,
        active: true,
    };

    user1.username = String::from("wallace123");

    let user2 = build_user(String::from("kyle@mail.com"), String::from("james123"));

    let user3 = User {
        email: String::from("kyle@mail.com"),
        username: String::from("james123"),
        ..user2
    };

    println!("{:#?}", user3);

    let rect = Rectangle {
        height: 12,
        width: 25,
    };
    println!("Here is our {:?}", rect);

    println!(
        "The area of the rectangle is {} square pixels.",
        rect.area()
    );
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}